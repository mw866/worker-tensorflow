# Tensorflow on Steroid

##tf.js
https://www.tensorflow.org/js/guide/nodejs

https://codelabs.developers.google.com/codelabs/tensorflowjs-teachablemachine-codelab/index.html


Install specific dependency not npm releases
`npm install --saved github:tensorflow/tfjs-models.git#707c3603972f9b08ce330ade5cabee1777d9f454`


## Rollup for Tensorflow.js
```
cd ~/dev/tfjs-models/mobilenet 
npm install
npm run build
```


##Webpack for main.js

Intall 
`npm install webpack webpack-cli workers-preview serverless-cloudflare-workers serverless --save-dev`

Run Webpack
`npx webpack --config webpack.config.js` OR `webpack` OR `npm run build`
 
https://webpack.js.org/guides/getting-started/
https://blog.cloudflare.com/using-webpack-to-bundle-workers/



```
~/dev/worker-tensorflow/dist   master ● ?  npm run build

> worker-tensorflow@1.0.0 build /Users/cwang/dev/worker-tensorflow
> webpack

Hash: c3650fce6a9681ce3851
Version: webpack 4.29.6
Time: 10953ms
Built at: 03/19/2019 5:22:15 PM
  Asset     Size  Chunks                    Chunk Names
main.js  986 KiB       0  [emitted]  [big]  main
Entrypoint main [big] = main.js
  [6] (webpack)/buildin/global.js 472 bytes {0} [built]
 [62] (webpack)/buildin/module.js 497 bytes {0} [built]
 [92] ./node_modules/@tensorflow-models/mobilenet/dist/mobilenet.esm.js + 24 modules 375 KiB {0} [built]
      |    25 modules
 [93] ./src/index.js 572 bytes {0} [built]
 [97] util (ignored) 15 bytes {0} [built]
 [99] util (ignored) 15 bytes {0} [built]
[133] buffer (ignored) 15 bytes {0} [optional] [built]
[134] crypto (ignored) 15 bytes {0} [optional] [built]
    + 192 hidden modules

WARNING in configuration
The 'mode' option has not been set, webpack will fallback to 'production' for this value. Set 'mode' option to 'development' or 'production' to enable defaults for each environment.
You can also set it to 'none' to disable any default behavior. Learn more: https://webpack.js.org/concepts/mode/

WARNING in asset size limit: The following asset(s) exceed the recommended size limit (244 KiB).
This can impact web performance.
Assets: 
  main.js (986 KiB)

WARNING in entrypoint size limit: The following entrypoint(s) combined asset size exceeds the recommended limit (244 KiB). This can impact web performance.
Entrypoints:
  main (986 KiB)
      main.js


WARNING in webpack performance recommendations: 
You can limit the size of your bundles by using import() or require.ensure to lazy load some parts of your application.
For more info visit https://webpack.js.org/guides/code-splitting/
```


## Cloudflare Workers
https://blog.cloudflare.com/using-webpack-to-bundle-workers/


Issues:

Change target from `web`(default) to `webworker`.
https://webpack.js.org/configuration/target/



```
npm run lint

> @tensorflow-models/mobilenet@0.2.2 lint /Users/cwang/dev/tfjs-models/mobilenet
> tslint -p . -t verbose

Failed to load /Users/cwang/dev/tfjs-models/tslint.json: Invalid "extends" configuration value - could not require "tslint-no-circular-imports". Review the Node lookup algorithm (https://nodejs.org/api/modules.html#modules_all_together) for the approximate method TSLint uses to find the referenced configuration file.
npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! @tensorflow-models/mobilenet@0.2.2 lint: `tslint -p . -t verbose`
npm ERR! Exit status 1
npm ERR! 
npm ERR! Failed at the @tensorflow-models/mobilenet@0.2.2 lint script.
npm ERR! This is probably not a problem with npm. There is likely additional logging output above.

npm ERR! A complete log of this run can be found in:
npm ERR!     /Users/cwang/.npm/_logs/2019-03-21T07_32_20_078Z-debug.log
```
Solution:
`npm install tslint-no-circular-imports`