import * as mobilenet from '/Users/cwang/dev/tfjs-models/mobilenet/dist/index.js';

const version = 2;
const alpha = 0.5;

addEventListener('fetch', event => {
  event.respondWith(app(event.request))
})

async function app(request) {

    // const img = document.getElementById('img');
    const img = await fetch(request.url)
    // Load the model.
    const model = await mobilenet.load(version, alpha);
    // Classify the image.
    const predictions = await model.classify(img);
    console.log('Predictions');
    console.log(predictions);
    // Get the logits.
    const logits = model.infer(img);
    console.log('Logits');
    logits.print(true);
    // Get the embedding.
    const embedding = model.infer(img, true);
    console.log('Embedding');
    embedding.print(true);
}
